import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.CartPage;
import pageObjects.HeaderComponent;
import pageObjects.ProductDetailPage;
import pageObjects.SearchResultsPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class AmazonTest {

    private WebDriver driver;
    private HeaderComponent headerComponent;

    @BeforeMethod
    public void testSetup() {
        //setup firefox driver
        WebDriverManager.firefoxdriver().setup();

        // open browser
        driver = new FirefoxDriver();
        driver.get("https://www.amazon.com");
        headerComponent = new HeaderComponent(driver);
    }

    @Test
    public void simpleSearchTest() {
        // search for a product
        SearchResultsPage searchResultsPage = headerComponent.search("Selenium WebDriver");

        // check search result info bar
        assertThat(searchResultsPage.getSearchResultsInfoBarText(),
                containsString("results for \"Selenium WebDriver\""));

        // check all displayed search results
        for (WebElement searchResultTitle : driver.findElements(By.cssSelector("[data-component-type=\"s-search-results\"] h5"))) {
            assertThat(searchResultTitle.getText().toLowerCase(),
                    containsString("selenium"));
        }
    }

    @Test
    public void addToCartTest() {
        // search for a product
        SearchResultsPage searchResultsPage = headerComponent.search("Selenium WebDriver");

        // open first search result
        ProductDetailPage productDetailPage = searchResultsPage.openNthSearchResult(1);

        // store product name for later verification
        String productTitle = productDetailPage.getProductTitle();

        // add to cart
        CartPage cartPage = productDetailPage.addToCart();

        // view cart
        cartPage.openCartContent();

        // verify cart item title equals to product name
        assertThat(cartPage.getNthProductTitle(1), containsString(productTitle));
    }

    @AfterMethod
    private void testTearDown() {
        // close browser
        driver.close();
    }
}
