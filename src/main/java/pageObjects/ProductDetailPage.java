package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ProductDetailPage {

    private WebDriver driver;

    @FindBy(id="productTitle")
    private WebElement productTitle;

    @FindBy(id="add-to-cart-button")
    private WebElement addToCartBtn;

    public ProductDetailPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getProductTitle(){
        return productTitle.getText();
    }

    public CartPage addToCart() {
        addToCartBtn.click();
        return new CartPage(driver);
    }
}
