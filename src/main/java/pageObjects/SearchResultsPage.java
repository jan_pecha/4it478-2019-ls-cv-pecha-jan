package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchResultsPage {

    private WebDriver driver;

    @FindBy(css="[data-component-type=\"s-result-info-bar\"]")
    private WebElement searchResultsInfoBar;

    @FindBy(css="[data-component-type=\"s-search-results\"] h5")
    private List<WebElement> searchResultTitles;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public String getSearchResultsInfoBarText(){
        return searchResultsInfoBar.getText();
    }

    public ProductDetailPage openNthSearchResult(int n){
        searchResultTitles.get(n-1).click();
        return new ProductDetailPage(driver);
    }
}
