package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderComponent {

    private WebDriver driver;

    @FindBy(id="twotabsearchtextbox")
    private WebElement searchField;

    @FindBy(css="form.nav-searchbar input[type='submit']")
    private WebElement searchBtn;

    public HeaderComponent(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public SearchResultsPage search(String searchedValue){
        searchField.sendKeys(searchedValue);
        searchBtn.click();
        return new SearchResultsPage(driver);
    }
}
